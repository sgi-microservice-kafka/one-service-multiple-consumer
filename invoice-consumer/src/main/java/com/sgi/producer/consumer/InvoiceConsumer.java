package com.sgi.producer.consumer;

import com.sgi.producer.InvoiceService;
import com.sgi.producer.common.Invoice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class InvoiceConsumer {
    private final Logger logger = LoggerFactory.getLogger(InvoiceConsumer.class);
    @Autowired
    private KafkaTemplate<Object, Object> template;

    @KafkaListener(topics = "invoice-topic") //, concurrency = "2")
    public void processInvoice(Invoice invoice) throws InterruptedException {
        logger.info("Processing : " + invoice.toString());
        Thread.sleep(3000);
        logger.info("Done: " + invoice.toString());
    }
}
