package com.sgi.producer.controller;

import com.sgi.producer.InvoiceService;
import com.sgi.producer.common.Invoice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class InvoiceController {
    private final Logger logger = LoggerFactory.getLogger(InvoiceService.class);

    public static ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
    @Autowired
    private KafkaTemplate<Object, Object> template;

    private Integer counter = 0;

    public InvoiceController() {
        invoiceList.add(new Invoice("1", "INV001", "POL001", "HOL001", "Badu", 1000));
        invoiceList.add(new Invoice("2", "INV002", "POL001", "HOL001", "Badu", 1000));
        invoiceList.add(new Invoice("3", "INV003", "POL002", "HOL001", "Badu", 1000));

        invoiceList.add(new Invoice("4", "INV004", "POL003", "HOL002", "Budi", 1000));
        invoiceList.add(new Invoice("5", "INV005", "POL003", "HOL002", "Budi", 1000));
    }

    @GetMapping(path = "/api/invoice")
    public ArrayList<Invoice> getAllInvoice() {
        logger.info("Gell All Invoice");
        return this.invoiceList;
    }

    @PostMapping(path = "/api/invoice/add")
    public void addNewInvoice(@RequestBody Invoice newInvoice) {
        logger.info("Add new invoice : " + newInvoice.toString());
        this.invoiceList.add(newInvoice);
    }

    @PostMapping(path = "/api/invoice/process-with-key")
    public String processInvoiceWithKey() {
        for(Invoice invoice:invoiceList){
            logger.info("Sending event : " + invoice.getInvoiceId());
            this.template.send("invoice-topic", invoice.getInvoiceId(), invoice);
        }

        logger.info("Invoice event sent...");
        return "On process";
    }

    @PostMapping(path = "/api/invoice/process-with-key-partition")
    public String processInvoiceWithKeyAndPartition() throws InterruptedException {
        for(Invoice invoice:invoiceList){
            logger.info("Sending event : " + invoice.getInvoiceId());
            this.template.send("invoice-topic", this.counter, ""+this.counter, invoice);
            this.counter++;
            if(this.counter>2){
                this.counter = 0;
            }
            Thread.sleep(500);
        }

        logger.info("Invoice event sent...");
        return "On process";
    }

    @PostMapping(path = "/api/invoice/process-without-key")
    public String processInvoiceWithoutKey() {
        for(Invoice invoice:invoiceList){
            logger.info("Sending event : " + invoice.getInvoiceId());
            this.template.send("invoice-topic", invoice);
        }
        logger.info("Invoice event sent...");
        return "On process";
    }
}
